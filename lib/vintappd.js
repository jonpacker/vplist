exports async function applyVintappdToProductList (products, ignoreCache, fullData) {
  const codes = Object.keys(products)
  const codesQuery = JSON.stringify({ ids: codes })
  const hash = md5(codesQuery)

  let beers = !ignoreCache ? await redis.getAsync(hash) : null

  if (beers) {
    beers = JSON.parse(beers)
  } else {
    const response = await fetch(`https://vintappd.jonpacker.com/vp/getBatch`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ids: codes })
    })
    beers = await response.json()
    await redis.setexAsync(hash, 59 * 60, JSON.stringify(beers))
  }

  for (const code of Object.keys(products)) {
    const match = beers[code]
    if (!match) continue
    products[code].bid = match.bid
    products[code].rating = match.rating_score
    products[code].ratingCount = match.rating_count
    products[code].style = match.beer_style
    products[code].untappdBrewery = match.brewery.brewery_name
    products[code].untappdName = match.beer_name
    products[code].abv = match.beer_abv
  }

  return products
}

