const Groupings = {
  distribution: {
    selector: beer => beer.selection,
    props: (selection) => {
      if (selection === 'basis') {
        return {
          title: 'Basisutvalget / National Distribution',
          desc: 'Distributed to all stores.'
        }
      }
      else if (selection === 'bu') {
        return {
          title: 'Bestillingsutvalget / Nationally Available',
          desc: 'Available to order online to all stores and for home delivery.'
        }
      }
      else if (selection === 'tu') {
        return {
          title: 'Tilleggsutvalget / Additional Selection',
          desc: 'These beers are available for individual stores to order. Beers in TU are most often once-off releases, but may appear later in Bestillingsutvalget.'
        }
      }
      else if (selection === 'su') {
        return {
          title: 'Spesialsutvalget / Special Selection',
          desc: 'Small lots of beers allocated to the "Beer+" stores for special releases. These beers are available to purchase online for pickup at your nearest "Beer+" on the day of release.'
        }
      }
      return {
        title: '',
        desc: ''
      }
    },
  },
  brewery: {
    selector: beer => beer.untappdBrewery || 'none',
    props: (brewery) => {
      if (brewery === 'none') {
        return {
          title: '​​​​​Not Linked',
          desc: "These beers aren't linked to Untappd (and Vinmonopolet only stores the brewery's name in the name of the beer :/)"
        }
      }
      return { title: brewery }
    }
  },
  style: {
    selector: beer => beer.category || 'none',
    props: (category) => {
      if (category === 'none') {
        return {
          title: 'Uncategorized'
        }
      }
      return { title: category }
    }
  }
}

export default Groupings
