import { useCallback } from 'react'
import classnames from 'classnames'
import memoize from 'lodash/memoize'

export default function Group ({ groups, selectedGroup, onSelectGroup }) {
  const _selectGroup = useCallback(memoize(group => () => {
    onSelectGroup(group)
  }), [onSelectGroup])
  return (
    <div className='legend'>
      {groups.map(group => (
        <div
          className={classnames('legend-category', {
            'legend-category-selected': selectedGroup === group
          })}
          key={group}
          onClick={_selectGroup(group)}
        >
          {group}
        </div>
      ))}
    </div>
  )
}
