import { useCallback } from 'react'
import classnames from 'classnames'
import memoize from 'lodash/memoize'

export default function StyleLegend ({ selectedCategory, onSelectCategory }) {
  const _selectStyle = useCallback(memoize(style => () => {
    onSelectCategory(style)
  }), [onSelectCategory])
  return (
    <div className='legend'>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'hoppy'
        })}
        data-style='india-pale-ale'
        onClick={_selectStyle('hoppy')}
      >
        Hoppy
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'sour'
        })}
        data-style='surol'
        onClick={_selectStyle('sour')}
      >
        Sour/Funky
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'dark'
        })}
        data-style='porter-stout'
        onClick={_selectStyle('dark')}
      >
        Dark
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'barley-wine'
        })}
        data-style='barley-wine'
        onClick={_selectStyle('barley-wine')}
      >
        Barley Wine
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'brown-ale'
        })}
        data-style='brown-ale'
        onClick={_selectStyle('brown-ale')}
      >
        Brown/Red Ale
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'light'
        })}
        data-style='lys-ale'
        onClick={_selectStyle('light')}
      >
        Light
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'belgian'
        })}
        data-style='klosterstil'
        onClick={_selectStyle('belgian')}
      >
        Belgian
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'mjod'
        })}
        data-style='mjod'
        onClick={_selectStyle('mjod')}
      >
        Mjød
      </div>
      <div
        className={classnames('legend-category', {
          ['legend-category-selected']: selectedCategory === 'other'
        })}
        data-style='spesial'
        onClick={_selectStyle('other')}
      >
        Other
      </div>
    </div>
  )
}
