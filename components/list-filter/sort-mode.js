import { useCallback } from 'react'
import classnames from 'classnames'
import memoize from 'lodash/memoize'

export default function SortOrder ({ selectedMode, onSelectMode, canSortByDateAdded }) {
  const _selectMode = useCallback(memoize(mode => () => {
    onSelectMode(mode)
  }), [onSelectMode])
  return (
    <div className='legend'>
      <div
        className={classnames('legend-category', {
          'legend-category-selected': selectedMode === 'alphabetical'
        })}
        onClick={_selectMode('alphabetical')}
      >
        Name
      </div>
      <div
        className={classnames('legend-category', {
          'legend-category-selected': selectedMode === 'rating'
        })}
        onClick={_selectMode('rating')}
      >
        Rating
      </div>
      <div
        className={classnames('legend-category', {
          'legend-category-selected': selectedMode === 'price'
        })}
        onClick={_selectMode('price')}
      >
        Price
      </div>
      {canSortByDateAdded ? (
        <div
          className={classnames('legend-category', {
            'legend-category-selected': selectedMode === 'time'
          })}
          onClick={_selectMode('time')}
        >
          Date Added
        </div>
      ) : null}
    </div>
  )
}
