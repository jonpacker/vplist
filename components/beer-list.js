import { useSelector } from 'react-redux'
import { useMemo } from 'react'
import Beer from './beer-list/beer'
import styles from '../styles/beer.module.scss'
import {getSortedFilteredBeers} from '../store/client/selectors'
import ListFilter from './list-filter'
import groupBy from 'lodash/groupBy'
import sortBy from 'lodash/sortBy'

const Groupings = {
  style: {
    selector: beer => beer.category || 'none',
    props: (category) => {
      if (category === 'none') {
        return {
          title: 'Uncategorized'
        }
      }
      return { title: category }
    }
  },
  brewery: {
    selector: beer => beer.untappdBrewery || 'none',
    props: (brewery) => {
      if (brewery === 'none') {
        return {
          title: '​​​​​Not Linked',
          desc: "These beers aren't linked to Untappd (and Vinmonopolet only stores the brewery's name in the name of the beer :/)"
        }
      }
      return { title: brewery }
    }
  },
  none: {
    selector: () => '',
    props: () => {
      return { title: '' }
    }
  }
}

export default function BeerList () {
  const beers = useSelector(getSortedFilteredBeers)
  const group = useSelector(state => Groupings[state.client.group || 'style'])
  const groupedBeers = useMemo(() => groupBy(beers, group.selector), [beers, group])
  return (
    <>
      <ListFilter groupKeys={Object.keys(Groupings)} defaultGroup='brewery' />
      {sortBy(Object.keys(groupedBeers), key => key.toLowerCase()).map(groupKey => {
        const props = group.props(groupKey)
        return (
          <div key={groupKey}>
            <h1>{props.title}</h1>
            {props.desc ? (
              <div className='h1desc'>{props.desc}</div>
            ) : null}
            <div className={styles.beerList}>
              {groupedBeers[groupKey].map(({ code }) => <Beer code={code} key={code} />)}
            </div>
          </div>
        )
      })}
    </>
  )
}
