import Legend from './list-filter/legend'
import { SELECT_CATEGORY, SELECT_SORT_ORDER, SELECT_SORT_MODE, SELECT_GROUP } from '../store/client/actions'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'
import SortOrder from './list-filter/sort-mode'
import Group from './list-filter/group'
import {isStoreListing} from '../store/server/selectors'

export default function ListFilter ({ groupKeys, defaultGroup }) {
  const dispatch = useDispatch()
  const selectedCategory = useSelector(state => state.client.selectedCategory)
  const sortMode = useSelector(state => state.client.sortMode)
  const sortOrder = useSelector(state => state.client.sortOrder)
  const group = useSelector(state => state.client.group) || defaultGroup
  const isStore = useSelector(isStoreListing)

  const _selectCategory = useCallback(category => {
    dispatch({ type: SELECT_CATEGORY, category: category === selectedCategory ? null : category })
  }, [selectedCategory])

  const _selectSortMode = useCallback(mode => {
    if (sortMode === mode) {
      dispatch({ type: SELECT_SORT_ORDER, sortOrder: sortOrder === 'asc' ? 'desc' : 'asc' })
    } else {
      dispatch({ type: SELECT_SORT_MODE, sortMode: mode })
    }
  }, [sortMode, sortOrder])

  const _selectGroup = useCallback(group => {
    dispatch({ type: SELECT_GROUP, group })
  }, [group])

  return (
    <div className='list-filter'>
      <h3>Filter By</h3>
      <Legend selectedCategory={selectedCategory} onSelectCategory={_selectCategory} />
      <h3>Sort By</h3>
      <SortOrder selectedMode={sortMode} onSelectMode={_selectSortMode} canSortByDateAdded={isStore} />
      <h3>Group By</h3>
      <Group groups={groupKeys} selectedGroup={group} onSelectGroup={_selectGroup} />
    </div>
  )
}
