import { SELECT_STORE } from '../store/client/actions'
import { useDispatch, useSelector } from 'react-redux'
import { useCallback } from 'react'
import classnames from 'classnames'
import memoize from 'lodash/memoize'

export default function StoreSelector ({ stores }) {
  const dispatch = useDispatch()
  const selectedStore = useSelector(state => state.client.selectedStore)

  const _selectStore = useCallback(memoize(store => () => {
    dispatch({ type: SELECT_STORE, store: store === selectedStore ? null : store })
  }), [selectedStore])

  return (
    <div className='list-filter'>
      <h3>Show Spesialutvalget Allocation For</h3>
      <div className='legend'>
        {stores.map(store => (
          <div
            key={store}
            className={classnames('legend-category', {
              'legend-category-selected': selectedStore === store
            })}
            onClick={_selectStore(store)}
          >
            {store}
          </div>
        ))}
      </div>
    </div>
  )
}
