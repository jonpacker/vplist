import { useSelector } from 'react-redux'
import styles from '../../styles/beer.module.scss'
import ColourScale from 'color-scales'
import classnames from 'classnames'
import formatDistanceToNow from 'date-fns/formatDistanceToNow'
import slug from 'slug'
import compact from 'lodash/compact'

const stops = [
  '#F2E963',
  '#F2A341',
  '#F27B35',
  '#F22E52',
  '#B430D9',
  '#7F46A6'
]

const selectionNames = {
  'su': 'Spesialutvalget',
  'tu': 'Tilleggsutvalget',
  'bu': 'Bestillingsutvalget',
  'basis': 'Basisutvalget'
}

const colourScale = new ColourScale(3.5, 4.7, stops)

const warningStops = ['#ff0000', '#000000']
const warningColourScale = new ColourScale(0, 500, warningStops)

const initialAppearedTime = Math.round(1611910808731/5000)

export default function Beer ({ code }) {
  const beer = useSelector(state => state.server.stock[code])
  const selectedStore = useSelector(state => state.client.selectedStore)
  const showSelection = useSelector(state => state.client.group !== 'distribution' || !state.client.group)

  if (!beer) return null

  let price = beer.price != null ? (
    <div
      className={styles.price}
      style={{
        '--price-woah-distance': beer.price / beer.volume / 0.181818,
        '--price-woah-clamped': Math.min(Math.max(beer.price / beer.volume / 0.181818 / 3 + 0.33, 0.9), 1.5)
      }}
    >
      {beer.price}kr
    </div>
  ) : null

  let allocation = beer.allocations && selectedStore ? beer.allocations[selectedStore] : null

  return (
    <div
      className={classnames(styles.beer, { [styles.unavailable]: allocation === 0 })}
      data-style={slug(beer.category || '')}
    >
      <div className={styles.title}>
        {beer.untappdBrewery ? (
          <>
            <h3>
              <div className={styles.breweryName}>
                {price}{beer.untappdBrewery}
              </div>
              {beer.untappdName}
            </h3>
            <div className={styles.linkedTo}>
              (Vinmonopolet: "{beer.name}")
            </div>
          </>
        ) : beer.brewery ? (
          <>
            <h3>
              <div className={styles.brewery}>
                {price}{beer.brewery}
              </div>
              {beer.name}
            </h3>
          </>
        ) : (
          <h3><div>{price}</div>{beer.name}</h3>
        )}
      </div>
      <div className={styles.rightColumn}>
        {beer.rating != null ? (
          <div
          className={classnames(styles.rating)}
            style={{
              '--rating-colour-0': colourScale.getColor(beer.rating - 0.05).toHexString(),
              '--rating-colour-1': colourScale.getColor(beer.rating + 0.05).toHexString(),
              '--warning-opacity': 1 - Math.min(1, beer.ratingCount / 500)
            }}
          >
            {beer.rating.toFixed(2)}
            <div
              className={styles.ratingCount}
              style={{
                '--warning-colour': warningColourScale.getColor(beer.ratingCount).toHexString()
              }}
            >
              {beer.ratingCount < 150 ? 'Only ' : ''}{beer.ratingCount}
            </div>
          </div>
        ) : (
          <div className={classnames(styles.rating, styles.unrated)}>
            Not Linked
            <span>({beer.code})</span>
          </div>
        )}
        <div className={styles.beerLinks}>
          {beer.bid ? (
            <a href={`https://untappd.com/b/_/${beer.bid}`} target='_blank' alt='Open in Untappd'>
              <img src='/untappd.svg' width='20' height='20' />
            </a>
          ) : null}
          <a href={`https://www.vinmonopolet.no/_/p/${beer.code}`} target='_blank' alt='Open on Vinmonopolet.no'>
            <img src='/vp.svg' width='20' height='20' />
          </a>
        </div>
      </div>
      <div className={styles.meta}>
        {showSelection ? (
          <div className={styles.selection}>
            {selectionNames[beer.selection]}
          </div>
        ) : null}
        <div className={styles.classification}>
          {compact([
            beer.style || beer.category,
            beer.abv ? `${beer.abv.toFixed(1)}%` : null,
            beer.volume ? `${beer.volume / 10}cl` : null
          ]).join(', ')}
        </div>
        {beer.stock != null ? (
          <div className={styles.stock}>
            {beer.stock} in stock{beer.time && Math.round(beer.time / 5000) !== initialAppearedTime ? `, appeared ${formatDistanceToNow(beer.time, { addSuffix: true })}` : ''}
          </div>
        ) : null}
        {allocation != null ? (
          <div className={classnames(styles.allocation, { [styles.noAllocation]: allocation === 0 })}>
            Allocation for {selectedStore}: {allocation}
          </div>
        ) : null}
        {beer.maxPerPerson != null ? (
          <div className={styles.quota}>
            Limit: {beer.maxPerPerson} per person
          </div>
        ) : null}
      </div>
    </div>
  )
}
