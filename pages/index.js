import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home({ stores }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>VP List</title>
      </Head>
      <h1>Vinmonopolet Releases</h1>
      <ul className={styles.releaseList}>
        <li><Link href={`/releases/juleol2021`}>Juleøl 2021</Link></li>
        <li><Link href={`/releases/november2021`}>November 2021</Link></li>
        <li><Link href={`/releases/september2021`}>September 2021</Link></li>
        <li><Link href={`/releases/june2021`}>June 2021</Link></li>
        <li><Link href={`/releases/may2021`}>May 2021</Link></li>
        <li><Link href={`/releases/april2021`}>April 2021</Link></li>
        <li><Link href={`/releases/march2021`}>March 2021</Link></li>
      </ul>
    </div>
  )
}

export async function getStaticProps () {
  const storesRequest = await fetch(`https://vpdiff.jonpacker.com/stores.json`)
  const stores = await storesRequest.json()
  return {
    props: {
      stores
    }
  }
}
