import Head from 'next/head'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { wrapper } from '../../store'
import { SET_STOCK } from '../../store/server/actions'
import Beer from '../../components/beer-list/beer'
import styles from '../../styles/store.module.scss'
import beerStyles from '../../styles/beer.module.scss'
import data from '../../release-data/juleøl2021.json'
import keyBy from 'lodash/keyBy'
import groupBy from 'lodash/groupBy'
import sortBy from 'lodash/sortBy'
import ListFilter from '../../components/list-filter'
import { getFilteredBeers, getSortedFilteredBeers } from '../../store/client/selectors'
import Groupings from '../../lib/releases/groupings'

export default function StoreStockListing () {
  const beers = useSelector(getSortedFilteredBeers)
  const group = useSelector(state => Groupings[state.client.group || 'style'])
  const groupedBeers = useMemo(() => groupBy(beers, group.selector), [beers, group])
  return (
    <div className={styles.storeStockListing}>
      <Head>
        <title>Juleøl 2021</title>
        <meta property="og:url" content="https://vplist.jonpacker.com/releases/juleøl2021" />
        <meta property="og:title" content="Juleøl 2021" />
        <meta property="og:image" content="https://vplist.jonpacker.com/prev.jpg" />
      </Head>
      <h1>Juleøl 2021 - November 3rd release</h1>
      <ListFilter groupKeys={Object.keys(Groupings)} defaultGroup='distribution' />
      {sortBy(Object.keys(groupedBeers), key => key.toLowerCase()).map(groupKey => {
        const props = group.props(groupKey)
        return (
          <div key={groupKey}>
            <h1>{props.title}</h1>
            {props.desc ? (
              <div className='h1desc'>{props.desc}</div>
            ) : null}
            <div className={beerStyles.beerList}>
              {groupedBeers[groupKey].map(({ code }) => <Beer code={code} key={code} />)}
            </div>
          </div>
        )
      })}
    </div>
  )
}

export const getStaticProps = wrapper.getStaticProps(async ({ params, store }) => {
  const stock = keyBy(data, b => b.code)
  store.dispatch({ type: SET_STOCK, stock, store: {} })
})

