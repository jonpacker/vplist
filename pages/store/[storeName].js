import Head from 'next/head'
import { getStoreStock, getStores } from '../../api/vpdiff'
import { applyVintappdToProductList } from '../../api/vintappd'
import { wrapper } from '../../store'
import { SET_STOCK } from '../../store/server/actions'
import { useSelector } from 'react-redux'
import BeerList from '../../components/beer-list'
import styles from '../../styles/store.module.scss'

export default function StoreStockListing () {
  const store = useSelector(state => state.server.store)
  return (
    <div className={styles.storeStockListing}>
      <Head>
        <title>VPL: {store.storeName}</title>
      </Head>
      <div><strong>Store Product Listing</strong></div>
      <h1>{store.storeName}</h1>
      <BeerList />
    </div>
  )
}


export async function getStaticPaths() {
  const stores = await getStores()
  return {
    paths: stores.map(({ storeKey }) => ({
      params: { storeName: storeKey }
    })),
    fallback: 'blocking'
  }
}

export const getStaticProps = wrapper.getStaticProps(async ({ params, store }) => {
  let stock = await getStoreStock(params.storeName)
  const stores = await getStores()
  stock = await applyVintappdToProductList(stock)
  const storeData = stores.find(store => store.storeKey === params.storeName)
  store.dispatch({ type: SET_STOCK, stock, store: storeData })
  return { props: { }, revalidate: 3600 }
})

