import '../styles/globals.scss'
import '../styles/list-filter.scss'
import { wrapper } from '../store'

function VPListApp ({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default wrapper.withRedux(VPListApp)
