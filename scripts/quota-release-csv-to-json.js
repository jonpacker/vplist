const csvtojson = require('csvtojson')
const fetch = require('node-fetch')

;(async () => {
  const data = await csvtojson({noheader: true, output:'csv'}).fromFile(process.argv[2])
  const allocationRows = {}
  const beerRows = await Promise.all(data.map(async (row, index) => {
    if (index === 0) {
      row.forEach((key, index) => {
        if (key && key.match(/fordeling\s/i)) {
          const [,store] = key.match(/fordeling\s(.+)$/i)
          allocationRows[index] = store
        }
        else if (key && key.match(/nettbutikk/i)) {
          allocationRows[index] = 'Online'
        }
      })
      return
    }
    if (!row[1] || isNaN(parseInt(row[1]))) return null
    const beer = {
      code: row[1],
      name: `${row[2]} ${row[3]}`,
      selection: 'su',
      price: parseFloat(row[8]),
      link: `https://www.vinmonopolet.no/_/p/${row[1]}`,
      category: row[5],
      country: row[4],
      volume: parseFloat(row[6]) * 1000,
      abv: parseFloat(row[7]),
      allocations: {}
    }

    Object.entries(allocationRows).forEach(([index, store]) => {
      beer.allocations[store] = parseInt(row[index])
    })

      /*
    if (row[25]) {
      const [, quotaStr] = row[25].match(/maks (\d+)/i)
      beer.maxPerPerson = parseInt(quotaStr)
    }
    */

    const req = await fetch(`https://vintappd.jonpacker.com/vp/${beer.code}`)
    const res = await req.json()
    if (res.code === 404) return beer

    beer.bid = res.beer.bid;
    beer.rating = res.beer.rating_score;
    beer.ratingCount = res.beer.rating_count;
    beer.style = res.beer.beer_style;
    beer.untappdBrewery = res.beer.brewery.brewery_name;
    beer.untappdName = res.beer.beer_name;
    beer.abv = beer.abv || res.beer.beer_abv;

    return beer
  }))

  process.stdout.write(JSON.stringify(beerRows.filter(b => b), null, ' '))
})()
