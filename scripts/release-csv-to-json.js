const csvtojson = require('csvtojson')
const fetch = require('node-fetch')

const selectionKey = {
  'tu': 'tu',
  'basis': 'basis',
  'bu': 'bu'
};

(async () => {
  const data = await csvtojson({delimiter: ';', noheader: true, output:'csv'}).fromFile(process.argv[2])
  const ids = data.map(row => row[0]).filter(id => !!id && !isNaN(parseInt(id, 10)))
  const batchRequest = await fetch(`https://vintappd.jonpacker.com/vp/getBatch`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ ids })
  })
  const linkedBeers = await batchRequest.json()

  const beerRows = await Promise.all(data.map(async row => {
    if (!row[0] || isNaN(parseInt(row[0]))) return null
    const beer = {
      code: row[0],
      name: row[1],
      selection: selectionKey[row[2]],
      price: parseFloat(row[7].replace(',','.')),
      link: `https://www.vinmonopolet.no/_/p/${row[0]}`,
      category: row[3],
      country: row[4],
      volume: parseFloat(row[6].replace(',', '.')) * 1000,
      abv: parseFloat(row[5].replace(',','.'))
    }

    const linkedBeer = linkedBeers[row[0]]

    if (!linkedBeer) return beer

    beer.bid = linkedBeer.bid;
    beer.rating = linkedBeer.rating_score;
    beer.ratingCount = linkedBeer.rating_count;
    beer.style = linkedBeer.beer_style;
    beer.untappdBrewery = linkedBeer.brewery.brewery_name;
    beer.untappdName = linkedBeer.beer_name;
    beer.abv = beer.abv || linkedBeer.beer_abv;

    return beer
  }))

  process.stdout.write(JSON.stringify(beerRows.filter(b => b), null, ' '))
})()
