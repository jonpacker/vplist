import { createSelector } from 'reselect'
import sortBy from 'lodash/sortBy'

const styleGroups = {
  'India pale ale': 'hoppy',
  'IPA': 'hoppy',
  'Ipa': 'hoppy',
  'Surøl': 'sour',
  'Porter & stout': 'dark',
  'Spesial': 'other',
  'Klosterstil': 'belgian',
  'Klosterøl': 'belgian',
  'Saison': 'sour',
  'Pale ale': 'hoppy',
  'Saison farmhouse ale': 'sour',
  'Lys lager': 'light',
  'Mørk lager': 'dark',
  'Stout': 'dark',
  'Barley wine': 'barley-wine',
  'Hveteøl': 'light',
  'Lys ale': 'light',
  'Lager': 'light',
  'Brown ale': 'brown-ale',
  'Red/amber': 'brown-ale',
  'Scotch ale': 'brown-ale',
  'Mjød': 'mjod'
}

export const getFilteredBeers = createSelector(
  state => state.server.stock,
  state => state.client.selectedCategory,
  (beers, category) => {
    if (!category) return Object.values(beers)
    return Object.values(beers).filter(beer => styleGroups[beer.category] === category)
  }
)

export const getSortedFilteredBeers = createSelector(
  getFilteredBeers,
  state => state.client.sortMode,
  state => state.client.sortOrder,
  (beers, mode, order) => {
    let sorted
    switch (mode) {
      case 'rating':
        sorted = sortBy(beers, b => b.rating ? 5 - b.rating : 5)
        break
      case 'price':
        sorted = sortBy(beers, b => b.price)
        break
      case 'time':
        sorted = sortBy(beers, b => -b.time)
        break
      case 'alphabetical':
      default: 
        sorted = sortBy(beers, b => b.untappdName || b.name)
    }
    if (order === 'asc') return sorted
    else {
      sorted.reverse()
      return sorted
    }
  }
)
