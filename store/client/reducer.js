import { SELECT_CATEGORY, SELECT_SORT_MODE, SELECT_SORT_ORDER, SELECT_GROUP, SELECT_STORE } from './actions'
const defaultState = {
  selectedCategory: null,
  sortMode: 'alphabetical',
  sortOrder: 'asc',
  selectedStore: null,
  group: null
}

export default function (state = defaultState, action) {
  switch (action.type) {
    case SELECT_CATEGORY:
      return { ...state, selectedCategory: action.category }
    case SELECT_SORT_MODE:
      return { ...state, sortMode: action.sortMode, sortOrder: action.sortOrder || state.sortOrder }
    case SELECT_SORT_ORDER:
      return { ...state, sortOrder: action.sortOrder }
    case SELECT_GROUP:
      return { ...state, group: action.group }
    case SELECT_STORE:
      return { ...state, selectedStore: action.store }
    default:
      return state
  }
}
