import { createStore, combineReducers } from 'redux'
import { createWrapper } from 'next-redux-wrapper'

import server from './server/reducer'
import client from './client/reducer'

const reducer = combineReducers({
  server,
  client
})

const makeStore = context => createStore(reducer)

export const wrapper = createWrapper(makeStore, { debug: true })
