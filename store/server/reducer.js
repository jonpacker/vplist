import { HYDRATE } from 'next-redux-wrapper'
import { SET_STOCK } from './actions'

const defaultState = {
  stock: {},
  store: {}
}

export default function (state = defaultState, action) {
  switch (action.type) {
    case HYDRATE:
      return {
        ...state,
        ...action.payload.server
      }
    case SET_STOCK:
      return {
        ...state,
        stock: action.stock,
        store: action.store
      }
    default:
      return state
  }
}
