export async function getStores () {
  const storesRequest = await fetch('https://vpdiff.jonpacker.com/stores.json')
  return storesRequest.json()
}

export async function getStoreStock (storeName) {
  const stockRequest = await fetch(`https://vpdiff.jonpacker.com/stock/${storeName}/json`)
  return stockRequest.json()
}
